# Stylize

Stylize injects your custom CSS stylesheets into existing HTML files, either as external `<link>` references, or embedded `<style>` sheets.

Stylize *does not* compile LESS, SASS, SCSS, Haml, Markdown, ReStructuredText, etc. It only operates on vanilla CSS and HTML.

## Usage

By default, Stylize will simply inject all specified CSS files into the specified HTML file in an embedded style tag, outputting to standard output.

Given two files, style.css

	body { font-family: 'Comic Sans MS'; }
	a { text-decoration: blink; }

And page.html

	<html>
	<head>
		<title>Some page</title>
		<link type="text/css" rel="stylesheet" href="base-styles.css">
	</head>
	<body>
		<h1>A page!</h1>
		<a href="#">Click me!</a>
	</body>
	</html>

We can use it like so:

	$ stylize page.html style.css
	<html>
	<head>
		<title>Some page</title>
		<link type="text/css" rel="stylesheet" href="base-styles.css">
		<style type="text/css">
			/* style.css */
			body { font-family: 'Comic Sans MS'; }
			a { text-decoration: blink; }
		</style>
	</head>
	<body>
		<h1>A page!</h1>
		<a href="#">Click me!</a>
	</body>
	</html>

Alternatively, you can pull the HTML file from standard input:

	$ cat page.html | stylize - style.css

Note that you need to add a `-` in place of file specification to let Stylize know it's reading from stdin.

You can also use `<link>` references instead.

	$ stylize --format=link page.html style.css
	<html>
	<head>
		<title>Some page</title>
		<link type="text/css" rel="stylesheet" href="base-styles.css">
		<link type="text/css" rel="stylesheet" href="style.css">
	</head>
	<body>
		<h1>A page!</h1>
		<a href="#">Click me!</a>
	</body>
	</html>

Note that the `href` attribute points to the location of stylesheet **relative to the html page**.

Finally, you can specify more than one CSS file, and they will be concatenated upon injection (or just listed, if using external stylesheets):

	$ stylize page.html style1.css style2.css
	<html>
	<head>
		<title>Some page</title>
		<link type="text/css" rel="stylesheet" href="base-styles.css">
		<style type="text/css">
			/* style1.css */
			body { font-family: 'Comic Sans MS'; }
			a { text-decoration: blink; }

			/* style2.css */
			h1 { color: #00F; }
		</style>
	</head>
	<body>
		<h1>A page!</h1>
		<a href="#">Click me!</a>
	</body>
	</html>

## Suggested Uses

I originally made Stylize because when transforming Markdown or similar formats to HTML, styles are left out. Stylize provides an easy, 
Unix-friendly way to add custom styles into the resulting document.

For example, with [documentify][documentify] and [browser][browser]:

	$ markdown README.md | documentify | stylize style.css | browser

[documentify]: https://bitbucket.org/austinhyde/documentify
[browser]: https://gist.github.com/318247