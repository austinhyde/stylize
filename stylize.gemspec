$LOAD_PATH << File.dirname(__FILE__) + '/lib'
require 'rubygems'
require 'stylize'


Gem::Specification.new do |spec|
  spec.name = 'stylize'
  spec.summary = 'Injects CSS into existing HTML files'
  spec.description = 'Stylize is a command-line utility that injects CSS into HTML documents'
  spec.author = 'Austin Hyde'
  spec.email = 'austin109@gmail.com'
  spec.homepage = 'http://bitbucket.org/austinhyde/stylize'

  spec.files = Dir['lib/*.rb'] + Dir['lib/**/*.rb'] + Dir['bin/*']
  spec.bindir = 'bin'
  spec.executables = ['stylize']
  spec.require_path = 'lib'

  spec.add_dependency 'nokogiri', '~> 1.5.0'

  spec.version = Stylize::VERSION
end