require 'optparse'
require 'rubygems'
require 'nokogiri'
require 'stylize/injector'

module Stylize
  VERSION = '0.1.1'

  VALID_FORMATS = [
    :style, # concatenates CSS files and injects inside <style> tag
    :link   # links to each CSS file with <link> tag
  ]

  DEFAULT_OPTIONS = {
    :format => :style
  }

  def self.stylize(html, css_files, opts={})
    doc = Nokogiri::HTML(html)

    head = (doc.root/:head).first || doc.root.add_child('<head></head>').first

    Stylize::Injector.for_format(opts[:format]).inject(head, css_files, opts)

    doc.to_s
  end

  def self.from_cli(argv)
    options = DEFAULT_OPTIONS.dup

    opt = OptionParser.new do |opts|
      opts.banner = "Usage: stylize [options] [page.html | -] styles.css [...]"

      opts.separator 'If reading from stdin, use a - (dash) in place of the html filename'
      opts.separator ''

      opts.on '-f FORMAT', '--format FORMAT', VALID_FORMATS, 'Select injection format' do |fmt|
        options[:format] = fmt
      end

      opts.on_tail '-h', '--help', 'Show this message' do
        puts opts
        exit
      end

      opts.on_tail '-v', '--version', 'Display version information' do
        puts "stylize #{Stylize::VERSION}"
        exit
      end
    end

    begin
      opt.parse! argv
    rescue OptionParser::InvalidOption => e
      $stderr.puts e
      $stderr.puts opt
      abort
    end

    if argv.length <= 1
      $stderr.puts "at least one input stylesheet is required"
      $stderr.puts opt
      abort
    end

    file = argv.shift

    if file == '-'
      input = $stdin.read
    else
      input = File.read(file)
    end

    print self.stylize(input, argv, options)
  end
end

require 'stylize/link_injector.rb'
require 'stylize/style_injector.rb'