require 'stylize/injector'

module Stylize
  class LinkInjector < Stylize::Injector
    register :link, self

    def inject(elem, files, opts={})
      files.each do |f|
        link = elem.add_element('link')
        link.add_attribute('type','text/css')
        link.add_attribute('rel','stylesheet')
        link.add_attribute('href',f)
      end
    end
  end
end