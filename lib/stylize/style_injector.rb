require 'stylize/injector'

module Stylize
  class StyleInjector < Stylize::Injector
    register :style, self

    def inject(elem, files, opts={})
      elem.add_child %{<style type="text/css">
#{files.map{|f| "/* #{f} */\n#{File.read(f)}"}.join("\n\n")}
</style>}
    end
  end
end