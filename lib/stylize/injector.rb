module Stylize
  class Injector
    @@injectors = {}
    def self.register(format, klass)
      @@injectors[format] = klass
    end

    def self.for_format(format)
      @@injectors[format].new
    end

    def inject(elem, files, opts={})
      raise NotImplementedError.new("Must subclass Injector and override inject()")
    end
  end
end